## Description

This sample code was used was used in my talk [Wrangling Kubernetes Configuration Management](https://docs.google.com/presentation/d/1K80h2ftuPVXF6s08G7jHYJvzwcYSPqNuoYaYBG7tCeM/edit?usp=sharing)

### Example 1

Supporting multiple icon paths in a `Chart.yaml`

To run:

```bash
$ ./example1/generate-chart.sh
```


### Example 2

Add a version label to existing Kubernetes resources within a single file

To run:

```bash
$ ./example2/add-version-label.sh <VERSION>
```
