local resources = std.parseJson(std.extVar('resources'));
local version = std.extVar('version');

local addVersionToMetadataLabels(resource) = resource + {
	metadata+: { labels+:
	 super.labels + { "app.kubernetes.io/version": version }
	}
};
local resourcesWithVersion = std.map(addVersionToMetadataLabels, resources);

{
  ["namespace.v" + version + ".json"]:
    std.filter(function(res) res.kind == "Namespace", resourcesWithVersion)[0],
  ["serviceaccount.v" + version + ".json"]:
    std.filter(function(res) res.kind == "ServiceAccount", resourcesWithVersion)[0]
}
