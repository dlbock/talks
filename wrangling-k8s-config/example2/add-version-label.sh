#!/bin/bash

set -e

VERSION=${1:-dev}
ROOT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/.."
SCRIPT_DIR="$ROOT_DIR/example2"
TARGET_DIR="$SCRIPT_DIR/target"

mkdir -p ${TARGET_DIR}

jsonnet \
  --ext-str resources="$($ROOT_DIR/yaml_to_json.py < $SCRIPT_DIR/my-application.yaml)" \
  --ext-str version=${VERSION} \
  -m ${TARGET_DIR} \
  ${SCRIPT_DIR}/my-application.jsonnet

for f in ${TARGET_DIR}/*.json
do
  [[ -f "$f" ]] || break
  ${ROOT_DIR}/json_to_yaml.py < $f > ${f%json}yaml
  rm $f
done

for f in ${TARGET_DIR}/*.yaml
do
  [[ -f "$f" ]] || break
  { echo "---" ; cat ${f}; } >> ${TARGET_DIR}/my-application.v${VERSION}.yaml
  rm ${f}
done
