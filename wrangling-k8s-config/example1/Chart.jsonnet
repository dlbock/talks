local type = std.extVar('type');
local localIcon = "file://../my-icon.png";
local remoteIcon = "https://remote-site.com/my-icon.png";
local iconPath = if type == 'rancher' then localIcon else remoteIcon;

{
  "apiVersion": "v1",
  "name": "my-awesome-application",
  "version": "1.0.28",
  "appVersion": 1.1,
  "description": "My Awesome Application",
  "home": "https://dlbock.github.io",
  "icon": iconPath,
  "sources": [
    "https://gitlab.com/dlbock/talks"
  ],
  "maintainers": [
    {
      "name": "dlbock",
      "email": "dlbock@gmail.com"
    }
  ]
}
