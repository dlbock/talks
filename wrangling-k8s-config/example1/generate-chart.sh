#!/bin/bash

set -e

ROOT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/.."
SCRIPT_DIR="$ROOT_DIR/example1"
TARGET_DIR="$SCRIPT_DIR/target"

mkdir -p ${TARGET_DIR}

jsonnet \
  --ext-str type=helm \
  -o ${TARGET_DIR}/Chart.helm.json \
  ${SCRIPT_DIR}/Chart.jsonnet

jsonnet \
  --ext-str type=rancher \
  -o ${TARGET_DIR}/Chart.rancher.json \
  ${SCRIPT_DIR}/Chart.jsonnet

for f in ${TARGET_DIR}/*.json
do
  [[ -f "$f" ]] || break
  ${ROOT_DIR}/json_to_yaml.py < $f > ${f%json}yaml
  rm $f
done
